FROM python:3.6
# ADD requirements.txt /tmp/requirements.txt
ADD . /code
WORKDIR /code
RUN pip install pipenv
RUN pipenv install --deploy --system
RUN pip install -e git+https://github.com/WisdomWolf/python-reminders@d6fd1dc14e7eb381fc55c0cdd0d3baf062f757fc#egg=python_reminders
RUN adduser --uid 1000 --disabled-password --gecos '' wisdomwolf && chown -R wisdomwolf:wisdomwolf /code
CMD ["python", "dog_fooder.py"]
