#!/usr/bin/env python3

import requests
from requests.exceptions import SSLError
from dateutil.parser import parse as dateparse
import pendulum
from pushbullet import Pushbullet
from telegram.ext import Updater

from ssl import SSLError
from apscheduler.schedulers.background import BlockingScheduler, BackgroundScheduler
import logging
from logging.config import fileConfig
from configparser import ConfigParser, NoSectionError
from json import JSONDecodeError
import sys
import os
import yaml
import jmespath
import argparse
from reminder import Reminder


# try:
#     fileConfig('./config/logging_config.ini')
# except KeyError:
#     pass
# if sys.flags.interactive:
#     logger = logging.getLogger('interactive')
# else:
#     logger = logging.getLogger('background')


def setup_logging(
        default_path='./config/logging_config.yaml',
        default_level=logging.INFO,
        env_key='LOG_CFG'
):
    """Setup logging configuration
    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


def get_last_fed(url, token):
    headers = {'Authorization': 'Bearer {}'.format(token), 'Content-Type': 'application/json'}
    url = f'{url}/api/states/sensor.dog_food'
    r = requests.get(url, headers=headers)
    try:
        last_fed = dateparse(r.json().get('state'))
        return pendulum.instance(last_fed, tz='US/Eastern').in_tz('UTC')
    except (JSONDecodeError, SSLError):
        logger.error('Unable to retrieve last fed from {}'.format(url))
        pushbullet_alert('The dogs might be hungry, unable to retrieve last fed to verify')
        return pendulum.now()


def is_dog_hungry(last_fed):
    # The dogs are considered hungry if 
    # they were last fed more than 3 hours ago
    return pendulum.now().add(hours=-3) > last_fed


def test_dog_hungry(*args, **kwargs):
    global test_count
    if test_count < 5:
        test_count += 1
        return True
    else:
        test_count = 0
        return False


def send_alerts(text):
    google_home_alert(text)
    telegram_alert(text)


def google_home_alert(text):
    logger.info('Alert submitted to Google Home')
    headers = {'Authorization': f'Bearer {hass_token}', 'Content-Type': 'application/json'}
    data = {'entity_id': 'media_player.hass_announcements', 'message': text}
    try:
        requests.post(f'{url}/api/services/tts/google_say', headers=headers, json=data)
    except requests.exceptions.SSLError:
        logger.error('Unable to send TTS alert')


def pushbullet_alert(text):
    pb.channels[0].push_note('Hungry dog alert!', text)

def telegram_alert(text):
    telegram_updater.bot.send_message(chat_id=telegram_chat_id, text=text)

def check():
    logger.debug('check() invoked')
    last_fed = get_last_fed(url, hass_token)
    if args.dryrun:
        checker = test_dog_hungry
    else:
        checker = is_dog_hungry
    if checker(last_fed):
        text = 'The dogs are hungry.  They were last fed {}'.format(last_fed.diff_for_humans())
        logger.warning(text)
        if not args.dryrun:
            send_alerts(text)
        else:
            logger.warning('would have sent: {}'.format(text))
        reminder.activate()
    else:
        logger.info('Dogs have already been fed, no alert necessary')
        reminder.cancel()


def setup(config_path=None):
    global reminder
    global pb
    global hass_token
    global url
    global telegram_updater
    global telegram_chat_id
    config_path = config_path or './config/config.yml'

    with open(config_path) as f:
        config = yaml.load(f)

    # Ensure logger is setup
    try:
        logger
    except NameError:
        setup_logging()
        logger = logging.getLogger(__name__)
    hass_token = jmespath.search('home_assistant.token', config)
    url = jmespath.search('home_assistant.url', config)
    pb_token = jmespath.search('pushbullet.token', config)
    pb = Pushbullet(pb_token)

    tg_token = jmespath.search('telegram.token', config)
    telegram_chat_id = jmespath.search('telegram.chat_id', config)
    telegram_updater = Updater(token=tg_token, use_context=True)

    reminder_units = jmespath.search('reminder.units', config)
    reminder_interval = jmespath.search('reminder.interval', config)

    reminder = Reminder(reminder_units, reminder_interval, check)
    schedules = jmespath.search('cron_schedules', config) or []
    scheduler = BlockingScheduler(timezone='US/Eastern')

    try:
        if args.dryrun:
            debug_job_time = pendulum.now().in_timezone('US/Eastern').add(minutes=2)
            schedules = [dict(func=check, trigger='cron', hour=debug_job_time.hour, minute=debug_job_time.minute)]
            scheduler.add_job(scheduler.print_jobs, 'date', run_date=pendulum.now().add(seconds=5))
    except NameError:
        pass

    for schedule in schedules:
        schedule['func'] = globals().get(schedule.get('func'), lambda: logger.error('unable to find {}'.format(schedule.get('func'))))
        logger.debug('adding new schedule: {}'.format(schedule))
        scheduler.add_job(misfire_grace_time=120, **schedule)
        logger.info('added cron job: {}'.format(schedule))
    logger.debug(scheduler.print_jobs())
    return scheduler


def main():
    global args
    global logger

    parser = argparse.ArgumentParser()
    parser.add_argument("--dryrun", help="Dry Run for testing",
                        action='store_true')
    args = parser.parse_args()

    setup_logging()
    logger = logging.getLogger(__name__)
    logger.info('Hungry Dog Alerter started')
    if args.dryrun:
        logger.warning('Running in Dry Run mode')
        logger.setLevel(logging.DEBUG)
        logger.debug('logger level set to DEBUG')
        scheduler = setup(config_path='debug_config.yml')
        global test_count
        test_count = 0
    else:
        scheduler = setup()
    # scheduler = BlockingScheduler(timezone='US/Eastern')
    # scheduler.add_job(check, 'cron', hour=7, minute=30, day_of_week='0-4')
    # scheduler.add_job(check, 'cron', hour=9, minute=45, day_of_week='5-6')
    # scheduler.add_job(check, 'cron', hour=20, minute=0)
    scheduler.start()


if __name__ == '__main__':
    main()

